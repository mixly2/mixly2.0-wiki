---
index: false
title: 控制
dir:
  order: 2
  text: 控制
---

控制类别中包括了时间延迟、条件执行、循环执行、获取运行时间、初始化、Switch执行等 控制模块中主要执行的内容是对程序结构进行的相应控制。

<Catalog />