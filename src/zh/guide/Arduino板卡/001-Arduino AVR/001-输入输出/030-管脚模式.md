---
title: 管脚模式
order: 30
category:
  - 输入/输出
---

## **管脚模式**

::: tabs#example

@tab 图形化#blocks

<img src="/assets/sw/inout/pinmode.png" alt="管脚模式" style="zoom:10%;" />

@tab 代码#code

```arduino
pinMode(2, INPUT);
```

:::

### **描述**

---

> 设置指定管脚的模式。

### **参数**

---

- 管脚: 引脚编号（如2，3）不同的开发板中断引脚不一样。
- 模式: 要将管脚设置成的模式，包括输入、输出、上拉输入。

