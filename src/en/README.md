---
home: true
icon: home
title: 项目主页
heroImage: /mixly.ico
bgImage: https://theme-hope-assets.vuejs.press/bg/6-light.svg
bgImageDark: https://theme-hope-assets.vuejs.press/bg/6-dark.svg
bgImageStyle:
  background-attachment: fixed
heroText: Mixly2.0 Wiki
tagline: Mixly2.0官方文档。
actions:
  - text: 文档
    link: ./guide/
    type: primary

copyright: false
footer: 使用 <a href="https://theme-hope.vuejs.press/zh/" target="_blank">VuePress Theme Hope</a> 主题 | MIT 协议, Copyright © Mixly Team@BNU, CHINA
---