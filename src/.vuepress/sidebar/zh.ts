import { sidebar } from "vuepress-theme-hope";

export const zhSidebar = sidebar({
  "/zh/guide/": [
    {
      text: "软件介绍",
      icon: "book",
      link: 'introduction.md'
    },
    {
      text: "Arduino板卡",
      prefix: 'Arduino板卡',
      children: "structure",
      collapsible: true
    },
    {
      text: "MicroPython板卡",
      prefix: 'MicroPython板卡',
      children: "structure",
      collapsible: true
    },
    {
      text: "CircuitPython板卡",
      prefix: 'CircuitPython板卡',
      children: "structure",
      collapsible: true
    },
  ],
  "/zh/develop/": [
    {
      text: "开发者文档",
      children: "structure",
      collapsible: true
    },
  ],
});
