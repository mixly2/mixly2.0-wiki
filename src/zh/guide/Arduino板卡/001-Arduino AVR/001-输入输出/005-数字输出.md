---
title: 数字输出
order: 5
category:
  - 输入/输出
---

## **数字输出**

::: tabs#example

@tab 图形化#blocks

<img src="/assets/sw/inout/digital-pin-output.png" alt="数字输出" style="zoom:10%;" />

@tab 代码#code

```arduino
digitalWrite(2, HIGH);
digitalWrite(2, LOW);
```

:::

### **描述**

---

> 给一个数字引脚写入HIGH或者LOW。

### **参数**

---

- 管脚: 引脚编号（如1，5，10，A0，A3）
- 值: 高 或 低

### **范例**

---

将13号端口设置为高电平，延迟一秒，然后设置为低电平，再延迟一秒，如此往复。

::: tabs#example

@tab 图形化#blocks

<img src="/assets/sw/inout/digital-pin-output-example.png" alt="数字输出示例" style="zoom:10%;" />

@tab 代码#code

```arduino
void setup(){
  pinMode(13, OUTPUT);
}

void loop(){
  digitalWrite(13,HIGH);
  delay(1000);
  digitalWrite(13,LOW);
  delay(1000);
}
```

:::

> [!warning]
> 数字13号引脚难以作为数字输入使用，因为大部分的控制板上使用了一颗LED与一个电阻连接到他。如果启动了内部的20K上拉电阻，他的电压将在1.7V左右，而不是正常的5V，因为板载LED串联的电阻把他使他降了下来，这意味着他返回的值总是LOW。如果必须使用数字13号引脚的输入模式，需要使用外部上拉下拉电阻