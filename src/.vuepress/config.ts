import { defineUserConfig } from "vuepress";
import theme from "./theme.js";
import { redirectPlugin } from '@vuepress/plugin-redirect';
import { registerComponentsPlugin } from '@vuepress/plugin-register-components';
import path from "path";

export default defineUserConfig({
  base: "/mixly2.0-wiki-dist/",
  locales: {
    "/zh/": {
      lang: "zh-CN",
      title: "Mixly2.0 Wiki",
      description: "Mixly2.0开发者文档",
    },
    "/en/": {
      lang: "en-US",
      title: "Mixly2.0 Wiki",
      description: "Mixly2.0 Wiki",
    },
  },
  plugins: [
    redirectPlugin({
      config: {
        '/': '/zh/',
      },
    }),
    registerComponentsPlugin({
      components: {
      },
    }),
  ],
  theme,
  // Enable it with pwa
  // shouldPrefetch: false,
});
