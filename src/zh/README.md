---
home: true
icon: home
title: 主页
heroImage: /mixly.ico
bgImage: /assets/image/6-light.svg
bgImageDark: /assets/image/6-dark.svg
bgImageStyle:
  background-attachment: fixed
heroText: Mixly2.0 Wiki
tagline: Mixly2.0官方文档。
actions:
  - text: 指南
    link: ./guide/
    type: primary
  - text: 开发者文档
    link: ./develop/
    type: primary

copyright: false
footer: 使用 <a href="https://theme-hope.vuejs.press/zh/" target="_blank">VuePress Theme Hope</a> 主题 | MIT 协议, Copyright © Mixly Team@BNU, CHINA
---
