---
index: false
title: 数学
dir:
  order: 3
  text: 数学
---

该部分主要完成数学变换功能，具体包括数字映射、数字约束、数学运算、取整、随机、三角函数。

<Catalog />